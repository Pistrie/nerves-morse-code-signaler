defmodule Morse do
  @moduledoc """
  Module to translate a string into morse code. For more information about morse code, see https://www.itu.int/dms_pubrec/itu-r/rec/m/R-REC-M.1677-1-200910-I!!PDF-E.pdf
  """

  # amount in ms
  @dit_time 300
  @dah_time 3 * @dit_time
  @between_words_time 7 * @dit_time

  @morse_dict %{
    "A" => ".-",
    "B" => "-...",
    "C" => "-.-.",
    "D" => "-..",
    "E" => ".",
    "F" => "..-.",
    "G" => "--.",
    "H" => "....",
    "I" => "..",
    "J" => ".---",
    "K" => "-.-",
    "L" => ".-..",
    "M" => "--",
    "N" => "-.",
    "O" => "---",
    "P" => ".--.",
    "Q" => "--.-",
    "R" => ".-.",
    "S" => "...",
    "T" => "-",
    "U" => "..-",
    "V" => "...-",
    "W" => ".--",
    "X" => "-..-",
    "Y" => "-.--",
    "Z" => "--..",
    "0" => "-----",
    "1" => ".----",
    "2" => "..---",
    "3" => "...--",
    "4" => "....-",
    "5" => ".....",
    "6" => "-....",
    "7" => "--...",
    "8" => "---..",
    "9" => "----.",
    "." => ".-.-.-",
    "," => "--..--",
    "?" => "..--..",
    "'" => ".----.",
    "!" => "-.-.--",
    "/" => "-..-.",
    "(" => "-.--.",
    ")" => "-.--.-",
    "&" => ".-...",
    ":" => "---...",
    ";" => "-.-.-.",
    "=" => "-...-",
    "+" => ".-.-.",
    "-" => "-....-",
    "_" => "..--.-",
    "\"" => ".-..-.",
    "$" => "...-..-",
    "@" => ".--.-.",
    " " => "/"
  }

  @doc """
  Translates a string containing valid ITU Morse code characters into morse code.
  Returns `{:ok, string}` if the input was valid, otherwise returns `:error`
  """
  def string_to_morse_code(input) when is_binary(input) do
    characters = String.split(input, "", trim: true)
    maybe_morse = for c <- characters, do: @morse_dict[c]

    if Enum.all?(maybe_morse),
      do: {:ok, maybe_morse |> Enum.join(" ")},
      else: :error
  end

  def get_morse_code_timings(morse_code) do
    for c <- morse_code |> String.graphemes() do
      # The function to manipulate the lights only concerns itself with just that.
      # We need to define the states ourselves. So after every on state comes an off state
      case c do
        "." -> [{:on, @dit_time}, {:off, @dit_time}]
        "-" -> [{:on, @dah_time}, {:off, @dit_time}]
        "/" -> {:off, @between_words_time}
        " " -> {:off, @dah_time}
      end
    end
    |> List.flatten()
    # after a dit or a dah comes an off state. If this is followed by a new letter or word there is an additional off state.
    # we need to get rid of multiple off states in a row, only keeping the one with the biggest value
    |> filter_multiple_off()
  end

  defp filter_multiple_off(list) do
    list
    |> Enum.reduce([], fn {state, time}, acc ->
      case acc do
        [{:off, prev_time} | rest] when state == :off and time > prev_time ->
          # replace prev_time with time
          [{:off, time} | rest]

        [{:off, prev_time} | _rest] when state == :off and time <= prev_time ->
          # don't add new time
          acc

        _ ->
          [{state, time} | acc]
      end
    end)
    |> Enum.reverse()
  end
end
