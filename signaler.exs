defmodule Signaler do
  @doc """
  Broadcast Morse code using LEDs.
  """

  alias Pigpiox.GPIO

  def morse_led_signaling(morse_code) when is_binary(morse_code) do
    timings = Morse.get_morse_code_timings(morse_code)
    leds = get_onboard_leds()

    # turn the leds off and wait a second
    for led <- leds do
      File.write(Path.join(led, "brightness"), "0")
    end

    :timer.sleep(1000)

    # execute the Morse code
    for {state, time} <- timings do
      control_lights(leds, {state, time})
    end

    # turn the leds back on after waiting a second
    :timer.sleep(1000)

    for led <- leds do
      File.write(Path.join(led, "brightness"), "1")
    end
  end

  def morse_gpio_signaling(morse_code) when is_binary(morse_code) do
    timings = Morse.get_morse_code_timings(morse_code)
    pins = get_gpio_pins()

    # set state to 0 just in case, then wait a second
    Enum.each(pins, &GPIO.write(&1, 0))

    :timer.sleep(1000)

    # execute the morse code
    for {state, time} <- timings do
      control_gpio(pins, {state, time})
    end
  end

  defp control_lights(leds, power_time_tuple) when is_list(leds) do
    case power_time_tuple do
      {:on, time} ->
        Enum.each(leds, &File.write(Path.join(&1, "brightness"), "1"))

        :timer.sleep(time)

      {:off, time} ->
        Enum.each(leds, &File.write(Path.join(&1, "brightness"), "0"))

        :timer.sleep(time)
    end
  end

  defp control_gpio(pins, power_time_tuple) when is_list(pins) do
    case power_time_tuple do
      {:on, time} ->
        Enum.each(pins, &GPIO.write(&1, 1))

        :timer.sleep(time)

      {:off, time} ->
        Enum.each(pins, &GPIO.write(&1, 0))

        :timer.sleep(time)
    end
  end

  defp get_onboard_leds do
    led_base_path = "/sys/class/leds"

    for led <- File.ls!(led_base_path) do
      Path.join(led_base_path, led)
    end
  end

  defp get_gpio_pins do
    # list of pins you want to interact with
    pins = [23]
    Enum.each(pins, &GPIO.set_mode(&1, :output))

    pins
  end
end
