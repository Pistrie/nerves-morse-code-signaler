# Morse code with Nerves

This repository contains some code that can make a raspberry pi send out morse code. It can either use the onboard LEDs or an external circuit using the GPIO pins.

## Usage

First install the Livebook firmware on your device. You can find more info on that here: https://github.com/livebook-dev/nerves_livebook

Then simply load the notebook included in this repository, change the pins inside `get_gpio_pins/0` to the ones you're using (if you don't want to use the onboard LEDs), and execute the appropriate cells

## Demo

![A video demonstrating an LED signaling "SOS" in Morse code](data/sos-morse-code.webm)

